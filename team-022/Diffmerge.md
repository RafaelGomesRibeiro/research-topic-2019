### EDOM - Project Research Topic

##### EMF - DIFFMERGE

###### Grupo 22





# �ndice



1. Introdu��o        

2. EMF - DiffMerge      

3. Exemplo de uma aplica��o     

4. Conclus�o     

5. Refer�ncias     

# �ndice de Figuras

Figura 1 - Processo de funcionamento da *tool*

Figura 2 - Compara��o dos 2 modelos

Figura 3 - Informa��o do que vai acontecer com o *merge*

Figura 4 - Resultado do *merge*


# 1. Introdu��o

A engenharia de dom�nio veio colmatar a lacuna que havia entre os *developers* de reutilizar modelos de dom�nio de outros softwares equivalentes, assim como a reutiliza��o de artefactos que fossem mais complicados de implementar. (Tracz, Coglianese, & Young, 1993).

Com o avan�o dessa engenharia surgiu a import�ncia de utilizar meta modelos, isto �, modelos de modelos, aparecendo assim a meta modela��o. Esta por sua vez baseia-se na an�lise, constru��o e desenvolvimento de *frames*, regras, *constraints*, modelos e teorias aplic�veis e importantes na modela��o.  

Assim sendo, o nosso trabalho consiste numa pesquisa sobre uma *tool*, o *EMF � Diffmerge*, para uma ferramenta que facilita o desenvolvimento de meta modelos, o *Eclipse Modeling Framework*.


# 2. EMF - DiffMerge

O *Eclipse Modeling Framework (EMF)* � uma *framework* para o *IDE (Integrated Development Environment) Eclipse*. Esta *framework* est� dividida em 3 fra��es fundamentais: 

1.	*EMF (Core)* que cont�m o meta-modelo (*Ecore*) para a descri��o de modelos e suporte em *runtime*, incluindo notifica��o de altera��es, suporte de persist�ncia com serializa��o XMI e uma API reflexiva muito eficiente para manipular objetos EMF.
2.	*MF (Edit)* que inclui classes gen�ricas reutiliz�veis para criar editores para modelos EMF.
3.	*EMF (Codegen)* � que possui um recurso de gera��o de c�digo EMF para tudo o que � necess�rio para criar uma estrutura completa para um modelo EMF. Inclui uma *GUI* a partir da qual as op��es para gera��o do c�digo podem ser especificadas. Baseia-se na utiliza��o do componente *JDT (Java Development Tooling)* do *Eclipse*.

O *merge* de modelos � bastante comum no decorrer de atividades baseadas em modelos. Contudo n�o se baseia s� no *merge* do c�digo por detr�s do modelo, torna-se um processo complexo devido ao facto dos modelos elaborados com o *EMF* serem constitu�dos por estruturas de data enriquecidas atrav�s dos meta-modelos e de algumas regras de engenharia.

Assim sendo, com o avan�o da utiliza��o dessa *framework* e com essa lacuna detetada surgiu uma *tool* para a compara��o de modelos, o *EMF - DiffMerge*, dando ent�o ao utilizador n�o s� a capacidade para comparar modelos, mas tamb�m a de poder fundi-los, num exemplo que iremos explicar mais � frente. Esta *tool* baseia-se numa ferramenta de *diff/merge* que est� desenhada para precaver a perda de dados de modo a tornar o modelo mais consistente e completo .

Na imagem que se segue, retirada do site do *EMF - DiffMerge*, est� descrito todo o processo de funcionamento desta *tool*.

![Diffmerge Flow](images/diffmerge_flow.png)
###### Figura 1 - Processo de funcionamento da *tool*

Todo o processo de *merge* de modelos baseia-se em 3 principais fases: compara��o, sele��o e *merge*. A ferramenta analisa os 2 modelos, verifica se existe compatibilidade entre eles, seleciona o que h� de diferente e, por fim, executa o *merge*, isto tudo respeitando as regras dos meta-modelos e a forma como foi anteriormente idealizado.

Esta *tool* apresenta uma desvantagem que pode ser crucial em grandes empresas, isto porque apenas consegue comprar 2 modelos. Assim sendo, em algumas empresas que t�m v�rias vers�es a serem trabalhadas em paralelo, n�o se pode comparar os modelos de todas.


# 3. Exemplo de uma aplica��o

Esta *tool* � bastante interessante na utiliza��o de reposit�rios de controlos de vers�es. Com um exemplo mais concreto vamos ent�o pegar no *mindmap* criado como exemplo nas aulas pr�ticas que se traduz numa agenda para contactos e eventos.

Com o avan�o do software foram introduzidas novas *features* a cada vers�o nova lan�ada. Contudo visto que v�rias pessoas estariam a trabalhar em *features* para vers�es diferentes, umas mais avan�adas que outras, existiria conflito de modelos de dom�nio. Assim sendo o *EMF - Diffmerge* possibilita essa compara��o podendo at� ao *developer* efetuar o *merge* entre os modelos de dom�nio.

![Ex1](images/ex1.png)  
###### Figura 2 - Compara��o dos 2 modelos

![Ex2](images/ex2.png)
###### Figura 3 - Informa��o do que vai acontecer com o *merge*

![Ex3](images/ex3.png)
###### Figura 4 - Resultado do *merge*

Explicando agora todo o fluxo de como foi realizado este exemplo, ap�s serem criados ambos os modelos, ambos muito b�sicos, apenas usando o exemplo fornecido nas aulas pr�ticas como treino para o trabalho pr�tico, conseguimos comparar os 2 numa segunda inst�ncia do eclipse, selecionando-os e utilizando a op��o de os comparar entre si como modelos.

Ap�s isso aparecer� uma janela de compara��o como a da Figura 2. Podemos ent�o verificar o que est� em falta em cada um dos modelos, vendo tamb�m qual � o principal para ser efetuado o *merge*.

Finalmente, ap�s a confirma��o das altera��es a serem aplicadas no *merge* dos modelos, essa mesma compara��o ir� ser atualizada para uma vista como a Figura 3 onde podemos verificar as altera��es feitas no modelo que escolhemos para realizar a fus�o dos mesmos.


# 4. Conclus�o

A engenharia de dom�nio, assim como o *framework EMF*, ganharam uma *tool* bastante interessante e bastante �til, visto ter proporcionado aos *developers* uma maior perce��o dos meta-modelos fazendo com que os mesmos pudessem essencialmente comparar modelos de vers�es.

Em suma, a lacuna identificada inicialmente conseguiu ser colmatada, contudo a n�o compara��o de mais do que 2 modelos trava um maior avan�o desta *tool* e da agiliza��o das atividades baseadas em modelos. Assim sendo uma introdu��o de uma nova *feature* como a compara��o de mais do que um modelo seria interessante, mas ter�amos a complexidade dos modelos e o limite de n�mero dos mesmos a n�o tornar f�cil essa nova funcionalidade.  


# 5. Refer�ncias

* Tracz, W., Coglianese, L., &amp; Young, P. (1993). A domain-specific software architecture engineering process outline. _ACM SIGSOFT Software Engineering Notes_, _18_(2), 40�49. https://doi.org/10.1145/159420.155837

* https://en.wikipedia.org/wiki/Metamodeling

* https://www.eclipse.org/modeling/emf/

* https://wiki.eclipse.org/EMF\_DiffMerge